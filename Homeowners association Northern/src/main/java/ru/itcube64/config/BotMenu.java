package ru.itcube64.config;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.itcube64.service.Action;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BotMenu extends TelegramLongPollingBot {
    private final Map<String, String> bindingBy = new ConcurrentHashMap<>();
    private final Map<String, Action> actions;
    private final String name;
    private final String token;

    public BotMenu(Map<String, Action> actions, String name, String token) {
        this.actions = actions;
        this.name = name;
        this.token = token;
    }

    @Override
    public String getBotUsername() {
        return name;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {

            String text = update.getMessage().getText();
            String chatId = update.getMessage().getChatId().toString();

            if (actions.containsKey(text)) {
                var message = actions.get(text).handle(update);
                bindingBy.put(chatId, text);
                send(message);
            } else if (bindingBy.containsKey(chatId)) {
                var message = actions.get(bindingBy.get(chatId)).callback(update);
                bindingBy.remove(chatId);
                send(message);
            }
        }
    }

    private void send(BotApiMethod message) {
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
