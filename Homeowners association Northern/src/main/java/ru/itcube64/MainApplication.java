package ru.itcube64;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import ru.itcube64.config.BotMenu;
import ru.itcube64.service.Action;
import ru.itcube64.service.impl.PassAction;
import ru.itcube64.service.impl.InfoAction;
import ru.itcube64.service.impl.CallAction;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MainApplication {
    public static void main(String[] args) throws TelegramApiException, IOException {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        Properties properties = new Properties();

        try (InputStream resourceAsStream = MainApplication.class
                .getClassLoader()
                .getResourceAsStream("application.properties")) {

            properties.load(resourceAsStream);
        }

        Map<String, Action> actions = Map.of(
                "/menu", new InfoAction(
                        List.of(
                                "/menu - Главное меню",
                                "/pass - Временный пропуск",
                                "/call - Заявка диспетчеру")
                ),
                "/pass", new PassAction("/pass", properties.getProperty("bot.chatId")),
                "/call", new CallAction()
        );

        telegramBotsApi.registerBot(new BotMenu(actions, properties.getProperty("bot.name"),
                properties.getProperty("bot.token")));
    }
}
