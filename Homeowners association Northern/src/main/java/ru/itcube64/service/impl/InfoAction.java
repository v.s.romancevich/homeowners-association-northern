package ru.itcube64.service.impl;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.itcube64.service.Action;

import java.util.List;

public class InfoAction implements Action {
    private final List<String> actions;

    public InfoAction(List<String> actions) {
        this.actions = actions;
    }

    @Override
    public BotApiMethod handle(Update update) {
        Message message = update.getMessage();
        String chatId = message.getChatId().toString();
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Выбирите действие:").append("\n");
        for (String action : actions) {
            stringBuilder.append(action).append("\n");
        }
        return new SendMessage(chatId, stringBuilder.toString());
    }

    @Override
    public BotApiMethod callback(Update update) {
        return handle(update);
    }
}
