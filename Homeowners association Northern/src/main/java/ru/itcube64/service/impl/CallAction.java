package ru.itcube64.service.impl;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.itcube64.service.Action;

public class CallAction implements Action {
    @Override
    public BotApiMethod handle(Update update) {
        Message message = update.getMessage();
        String chatId = message.getChatId().toString();
        String text = "Введите почту для регистрации нового пользователя:";

        return new SendMessage(chatId, text);
    }

    @Override
    public BotApiMethod callback(Update update) {
        Message message = update.getMessage();
        String chatId = message.getChatId().toString();
        String email = message.getText();
        // userRepository.save(new User(email));
        String text = "Пользователь успешно зарегистрирован: " + email;
        return new SendMessage(chatId, text);
    }
}
