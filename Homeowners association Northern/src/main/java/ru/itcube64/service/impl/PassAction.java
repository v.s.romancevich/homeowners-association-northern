package ru.itcube64.service.impl;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.itcube64.service.Action;

public class PassAction implements Action {
    private final String actions;
    private final String properties;

    public PassAction(String actions, String properties) {
        this.actions = actions;
        this.properties = properties;
    }

    @Override
    public BotApiMethod handle(Update update) {
        Message message = update.getMessage();
        String chatId = message.getChatId().toString();
        String text = "Введите номер автомобиля и место разгрузки:";

        return new SendMessage(chatId, text);
    }

    @Override
    public BotApiMethod callback(Update update) {
        Message message = update.getMessage();
        String chatId = message.getChatId().toString();

        String text = "Ваш пропуск: " + message.getChatId();

        return new SendMessage(chatId, text);
    }
}
